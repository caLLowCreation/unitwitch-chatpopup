﻿#region Author
/*
     
     Jones St. Lewis Cropper (caLLow)
     
     Another caLLowCreation
     
     Visit us on Google+ and other social media outlets @caLLowCreation
     
     Thanks for using our product.
     
     Send questions/comments/concerns/requests to 
      e-mail: caLLowCreation@gmail.com
      subject: UniTwitchChatPopup
     
*/
#endregion

using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UniTwitchChatPopup.Properties;

namespace UniTwitchChatPopup
{
    public partial class frmMain : Form
    {
        Regex m_TitleRegex;

        public frmMain()
        {
            LoadMainForm();
        }

        public frmMain(string[] args)
        {
            LoadMainForm(args);
        }

        void txtChannelName_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                NavigateToChatChannel((sender as TextBox).Text);
            }
        }

        void trkOpacity_Scroll(object sender, EventArgs e)
        {
            SaveOpacityDefaults((sender as TrackBar).Value);
        }

        void chkTopMost_CheckedChanged(object sender, EventArgs e)
        {
            SaveTopmostDefaults((sender as CheckBox).Checked);
        }

        void chatWebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            SaveChannelFromWebpage();
        }

        void chatWebBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            SaveChannelFromWebpage();
        }

        void LoadMainForm(params string[] args)
        {
            // Initialize
            InitializeComponent();
            this.m_TitleRegex = new Regex(Settings.Default.ROOM_TITLE_PATTERN);

            if (args != null && args.Length == 1)
            {
                if (!string.IsNullOrEmpty(args[0]))
                {
                    Settings.Default.Channel = args[0];
                }
            }

            // Load Component Defaults
            if (Settings.Default.Opacity < this.trkOpacity.Minimum) Settings.Default.Opacity = this.trkOpacity.Minimum;
            if (Settings.Default.Opacity > this.trkOpacity.Maximum) Settings.Default.Opacity = this.trkOpacity.Maximum;
            this.chkTopMost.Checked = Settings.Default.Topmost;
            this.txtChannelName.Text = Settings.Default.Channel;
            this.TopMost = Settings.Default.Topmost;
            this.trkOpacity.Value = Settings.Default.Opacity;
            this.Opacity = Settings.Default.Opacity / 100.0;
            this.Update();

            // Load Chat
            NavigateToChatChannel(Settings.Default.Channel);

        }

        void SaveChannelFromWebpage()
        {
            IEnumerator enumeator = this.chatWebBrowser.Document.GetElementsByTagName(Settings.Default.TITLE_ELEMENT_NAME).GetEnumerator();
            while (enumeator.MoveNext())
            {
                HtmlElement element = enumeator.Current as HtmlElement;
                if (!string.IsNullOrEmpty(element.OuterHtml) && m_TitleRegex.IsMatch(element.OuterHtml))
                {
                    element.DoubleClick -= OnTitleDoubleClick;
                    element.DoubleClick += OnTitleDoubleClick;

                    SaveChannelDefaults(element.InnerText);
                    break;
                }

            }
        }

        void OnTitleDoubleClick(object sender, HtmlElementEventArgs e)
        {
            this.txtChannelName.Visible = true;
            this.txtChannelName.Focus();
        }

        void SaveTopmostDefaults(bool isChecked)
        {
            this.chkTopMost.Checked = isChecked;
            this.TopMost = isChecked;
            Settings.Default.Topmost = isChecked;
            Settings.Default.Save();
        }

        void SaveChannelDefaults(string channel)
        {
            if (!string.IsNullOrEmpty(channel))
            {
                this.txtChannelName.Text = channel;
                Settings.Default.Channel = channel;
            }
            Settings.Default.Save();
        }

        void SaveOpacityDefaults(int value)
        {
            this.Opacity = value / 100.0;
            Settings.Default.Opacity = value;
            Settings.Default.Save();
        }

        void NavigateToChatChannel(string channel)
        {
            if (string.IsNullOrEmpty(channel)) return;
            this.txtChannelName.Visible = false;
            this.chatWebBrowser.Focus();
            this.chatWebBrowser.Navigate(string.Format(Settings.Default.URL_FORMAT, channel));
        }

    }
}
