﻿namespace UniTwitchChatPopup
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.chatWebBrowser = new System.Windows.Forms.WebBrowser();
            this.chkTopMost = new System.Windows.Forms.CheckBox();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.trkOpacity = new System.Windows.Forms.TrackBar();
            this.txtChannelName = new System.Windows.Forms.TextBox();
            this.pnlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkOpacity)).BeginInit();
            this.SuspendLayout();
            // 
            // chatWebBrowser
            // 
            this.chatWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chatWebBrowser.Location = new System.Drawing.Point(0, 0);
            this.chatWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.chatWebBrowser.Name = "chatWebBrowser";
            this.chatWebBrowser.ScriptErrorsSuppressed = true;
            this.chatWebBrowser.ScrollBarsEnabled = false;
            this.chatWebBrowser.Size = new System.Drawing.Size(292, 473);
            this.chatWebBrowser.TabIndex = 0;
            this.chatWebBrowser.Url = new System.Uri("", System.UriKind.Relative);
            this.chatWebBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.chatWebBrowser_DocumentCompleted);
            this.chatWebBrowser.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.chatWebBrowser_Navigated);
            // 
            // chkTopMost
            // 
            this.chkTopMost.AutoSize = true;
            this.chkTopMost.BackColor = System.Drawing.Color.Transparent;
            this.chkTopMost.Checked = true;
            this.chkTopMost.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTopMost.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkTopMost.Location = new System.Drawing.Point(3, -2);
            this.chkTopMost.Name = "chkTopMost";
            this.chkTopMost.Size = new System.Drawing.Size(64, 17);
            this.chkTopMost.TabIndex = 1;
            this.chkTopMost.Text = "Topmost";
            this.chkTopMost.UseVisualStyleBackColor = false;
            this.chkTopMost.CheckedChanged += new System.EventHandler(this.chkTopMost_CheckedChanged);
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.Color.Transparent;
            this.pnlHeader.Controls.Add(this.chkTopMost);
            this.pnlHeader.Controls.Add(this.trkOpacity);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(292, 15);
            this.pnlHeader.TabIndex = 2;
            // 
            // trkOpacity
            // 
            this.trkOpacity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.trkOpacity.AutoSize = false;
            this.trkOpacity.Location = new System.Drawing.Point(73, -1);
            this.trkOpacity.Maximum = 99;
            this.trkOpacity.Minimum = 20;
            this.trkOpacity.Name = "trkOpacity";
            this.trkOpacity.Size = new System.Drawing.Size(220, 18);
            this.trkOpacity.TabIndex = 3;
            this.trkOpacity.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trkOpacity.Value = 50;
            this.trkOpacity.Scroll += new System.EventHandler(this.trkOpacity_Scroll);
            // 
            // txtChannelName
            // 
            this.txtChannelName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChannelName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChannelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChannelName.Location = new System.Drawing.Point(12, 15);
            this.txtChannelName.Name = "txtChannelName";
            this.txtChannelName.Size = new System.Drawing.Size(268, 20);
            this.txtChannelName.TabIndex = 2;
            this.txtChannelName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtChannelName.Visible = false;
            this.txtChannelName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtChannelName_KeyUp);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 473);
            this.Controls.Add(this.txtChannelName);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.chatWebBrowser);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(256, 300);
            this.Name = "frmMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "UniTwitch ChatPopup";
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkOpacity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser chatWebBrowser;
        private System.Windows.Forms.CheckBox chkTopMost;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.TextBox txtChannelName;
        private System.Windows.Forms.TrackBar trkOpacity;
    }
}

