# README #

UniTwitch Chat Popup

### What is this repository for? ###

* UniTwitch Chat Popup is a light-weight chat window for use with UniTwitch IRC. UniTwitch Chat Popup is a web browser in a windows form. Simple and light on the memory usage, less than 70,000 K memory 0 CPU, as compared with a Twitch Chat pop out from Chrome using 80,000+ K in memory and 2-5 CPU usage.
* Version: 1.0.0

### How do I get set up? ###

* Include the files in a new Visual Studio Project

### Who do I talk to? ###

[Contact caLLowCreation](http://callowcreation.com/home/contact-us/)